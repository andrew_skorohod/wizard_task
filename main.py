from pymongo import MongoClient
import os
import sqlite3

from flask import Flask,render_template
app = Flask(__name__)


client = MongoClient()
db = client.test
zips = db.zips
items =[]
for zipCode in zips.find().sort("pop"):
    items.append(zipCode)
top20 = items[-20:]
for i in top20:
    i["population"]= i["pop"]
#print (top20)

@app.route('/')
def main():
    return render_template('index.html', zips=top20)

if __name__ == '__main__':
    app.run()
